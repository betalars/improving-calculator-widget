# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Martin Schlander <mschlander@opensuse.org>, 2010, 2015.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-28 00:50+0000\n"
"PO-Revision-Date: 2015-08-26 20:23+0200\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: datetimerunner.cpp:21
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "date"
msgstr "dato"

#: datetimerunner.cpp:22
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "time"
msgstr "tid"

#: datetimerunner.cpp:29
#, kde-format
msgid "Displays the current date"
msgstr "Viser den aktuelle dato"

#: datetimerunner.cpp:30
#, kde-format
msgid "Displays the current time"
msgstr "Viser den aktuelle tid"

#: datetimerunner.cpp:31 datetimerunner.cpp:33
#, kde-format
msgctxt "The <> and space are part of the example query"
msgid " <timezone>"
msgstr ""

#: datetimerunner.cpp:32
#, fuzzy, kde-format
#| msgid "Displays the current date in a given timezone"
msgid ""
"Displays the current date and difference to system date in a given timezone"
msgstr "Viser den aktuelle dato i en given tidszone"

#: datetimerunner.cpp:34
#, fuzzy, kde-format
#| msgid "Displays the current time in a given timezone"
msgid ""
"Displays the current time and difference to system time in a given timezone"
msgstr "Viser den aktuelle tid i en given tidszone"

#: datetimerunner.cpp:48
#, kde-format
msgid "Today's date is %1"
msgstr "Dags dato er %1"

#: datetimerunner.cpp:63
#, kde-format
msgctxt ""
"date difference between time zones, e.g. in Stockholm it's 1 calendar day "
"later than in Brasilia"
msgid "%1 later"
msgstr ""

#: datetimerunner.cpp:67
#, kde-format
msgctxt ""
"date difference between time zones, e.g. in Brasilia it's 1 calendar day "
"earlier than in Stockholm"
msgid "%1 earlier"
msgstr ""

#: datetimerunner.cpp:69
#, kde-format
msgctxt ""
"no date difference between time zones, e.g. in Stockholm it's the same "
"calendar day as in Berlin"
msgid "no date difference"
msgstr ""

#: datetimerunner.cpp:81
#, kde-format
msgid "Current time is %1"
msgstr "Klokken er %1"

#: datetimerunner.cpp:102
#, kde-format
msgctxt ""
"time difference between time zones, e.g. in Stockholm it's 4 hours later "
"than in Brasilia"
msgid "%1 later"
msgstr ""

#: datetimerunner.cpp:105
#, kde-format
msgctxt ""
"time difference between time zones, e.g. in Brasilia it's 4 hours ealier "
"than in Stockholm"
msgid "%1 earlier"
msgstr ""

#: datetimerunner.cpp:107
#, kde-format
msgctxt ""
"no time difference between time zones, e.g. in Stockholm it's the same time "
"as in Berlin"
msgid "no time difference"
msgstr ""

#~ msgid "The date in %1 is %2"
#~ msgstr "Datoen i %1 er %2"

#~ msgid "The current time in %1 is %2"
#~ msgstr "Den aktuelle tid i %1 er %2"
