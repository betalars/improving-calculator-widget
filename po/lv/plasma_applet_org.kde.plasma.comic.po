# translation of plasma_applet_comic.po to Latvian
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Maris Nartiss <maris.kde@gmail.com>, 2008.
# Viesturs Zarins <viesturs.zarins@mii.lu.lv>, 2008, 2010.
# Viesturs Zariņš <viesturs.zarins@mii.lu.lv>, 2009.
# Einars Sprugis <einars8@gmail.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_comic\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-27 00:48+0000\n"
"PO-Revision-Date: 2012-07-05 21:11+0300\n"
"Last-Translator: Einars Sprugis <einars8@gmail.com>\n"
"Language-Team: Latvian <locale@laka.lv>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.4\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"

#: comic.cpp:87
#, fuzzy, kde-format
#| msgctxt "here strip means comic strip"
#| msgid "&Next Tab with a new Strip"
msgctxt "@action comic strip"
msgid "&Next Tab with a New Strip"
msgstr "&Nākamā cilne ar jaunu lentu"

#: comic.cpp:93
#, fuzzy, kde-format
#| msgid "Jump to &first Strip"
msgctxt "@action"
msgid "Jump to &First Strip"
msgstr "Iet uz &pirmo lentu"

#: comic.cpp:97
#, fuzzy, kde-format
#| msgid "Jump to &current Strip"
msgctxt "@action"
msgid "Jump to &Current Strip"
msgstr "Iet uz &jaunāko lentu"

#: comic.cpp:101
#, fuzzy, kde-format
#| msgid "Jump to Strip ..."
msgctxt "@action"
msgid "Jump to Strip…"
msgstr "Iet uz lentu..."

#: comic.cpp:105
#, fuzzy, kde-format
#| msgid "Visit the shop &website"
msgctxt "@action"
msgid "Visit the Website"
msgstr "Doties uz veikala &tīmekļa lapu"

#: comic.cpp:109
#, kde-format
msgctxt "@action:inmenu %1 is the name of a web browser"
msgid "View in %1"
msgstr ""

#: comic.cpp:117
#, fuzzy, kde-format
#| msgid "Visit the shop &website"
msgctxt "@action"
msgid "Visit the Shop &Website"
msgstr "Doties uz veikala &tīmekļa lapu"

#: comic.cpp:122
#, fuzzy, kde-format
#| msgid "&Save Comic As..."
msgctxt "@action"
msgid "&Save Comic As…"
msgstr "&Saglabāt komiksu kā..."

#: comic.cpp:127
#, kde-format
msgctxt "@option:check Context menu of comic image"
msgid "&Actual Size"
msgstr "&Patiesais izmērs"

#: comic.cpp:135
#, fuzzy, kde-format
#| msgctxt "@option:check Context menu of comic image"
#| msgid "Store current &Position"
msgctxt "@option:check Context menu of comic image"
msgid "Store Current &Position"
msgstr "Saglabāt šobrīdējo &novietojumu"

#: comicdata.cpp:86
#, kde-format
msgctxt "an abbreviation for Number"
msgid "# %1"
msgstr "# %1"

#: package/contents/config/config.qml:13
#, fuzzy, kde-format
#| msgid "General"
msgctxt "@title"
msgid "General"
msgstr "Pamata"

#: package/contents/config/config.qml:18
#, fuzzy, kde-format
#| msgid "Appearance"
msgctxt "@title"
msgid "Appearance"
msgstr "Izskats"

#: package/contents/config/config.qml:23
#, fuzzy, kde-format
#| msgid "Advanced"
msgctxt "@title"
msgid "Advanced"
msgstr "Paplašināti"

#: package/contents/ui/ComicBottomInfo.qml:60
#, fuzzy, kde-format
#| msgid "Jump to Strip ..."
msgctxt "@info:tooltip"
msgid "Jump to strip…"
msgstr "Iet uz lentu..."

#: package/contents/ui/ComicBottomInfo.qml:100
#, fuzzy, kde-format
#| msgid "Visit the comic website"
msgctxt "@info:tooltip"
msgid "Visit the comic website"
msgstr "Apmeklēt komiksa lapu"

#: package/contents/ui/configAdvanced.qml:36
#, fuzzy, kde-format
#| msgid "Comic cache:"
msgctxt "@label:spinbox"
msgid "Comic cache:"
msgstr "Komiksu kešatmiņa:"

#: package/contents/ui/configAdvanced.qml:45
#, fuzzy, kde-format
#| msgid " strips per comic"
msgctxt "@item:valuesuffix spacing to number + unit"
msgid "strip per comic"
msgid_plural "strips per comic"
msgstr[0] " lentas komiksā"
msgstr[1] " lentas komiksā"
msgstr[2] " lentas komiksā"

#: package/contents/ui/configAdvanced.qml:51
#, fuzzy, kde-format
#| msgid "Display error image when getting comic failed:"
msgctxt "@option:check"
msgid "Display error when downloading comic fails"
msgstr "Parādīt kļūdas attēlu, kad komiksa ielāde neizdodas:"

#: package/contents/ui/configAppearance.qml:43
#, kde-format
msgctxt "Heading for showing various elements of a comic"
msgid "Show:"
msgstr ""

#: package/contents/ui/configAppearance.qml:44
#, fuzzy, kde-format
#| msgid "Show comic &title:"
msgctxt "@option:check"
msgid "Comic title"
msgstr "Rādīt komiksa &nosaukumu:"

#: package/contents/ui/configAppearance.qml:50
#, fuzzy, kde-format
#| msgid "Show comic &identifier:"
msgctxt "@option:check"
msgid "Comic identifier"
msgstr "Rādīt komiksa &identifikatoru:"

#: package/contents/ui/configAppearance.qml:56
#, fuzzy, kde-format
#| msgid "Show comic &author:"
msgctxt "@option:check"
msgid "Comic author"
msgstr "Rādīt komiksa &autoru:"

#: package/contents/ui/configAppearance.qml:62
#, fuzzy, kde-format
#| msgid "Comic"
msgctxt "@option:check"
msgid "Comic URL"
msgstr "Komikss"

#: package/contents/ui/configAppearance.qml:72
#, kde-format
msgid "Show navigation buttons:"
msgstr ""

#: package/contents/ui/configAppearance.qml:73
#, kde-format
msgctxt "@option:check"
msgid "Always"
msgstr ""

#: package/contents/ui/configAppearance.qml:80
#, kde-format
msgctxt "@option:check"
msgid "Only on hover"
msgstr ""

#: package/contents/ui/configGeneral.qml:52
#, fuzzy, kde-format
#| msgid "Comic"
msgctxt "@title:group"
msgid "Comics:"
msgstr "Komikss"

#: package/contents/ui/configGeneral.qml:72
#, fuzzy, kde-format
#| msgid "&Get New Comics..."
msgctxt "@action:button"
msgid "Get New Comics…"
msgstr "&Ielādēt jaunus komiksus..."

#: package/contents/ui/configGeneral.qml:83
#, fuzzy, kde-format
#| msgid "&Middle-click on the comic to show it at its original size"
msgctxt "@option:check"
msgid "Middle-click on comic to display at original size"
msgstr "&Vidējais klikšķis uz komiksa parāda to orģinālajā izmērā"

#: package/contents/ui/configGeneral.qml:92
#, fuzzy, kde-format
#| msgid "Check for new comic strips:"
msgctxt "@label:spinbox"
msgid "Check for new plugins every:"
msgstr "Pārbaudīt jaunas komiksa lentas:"

#: package/contents/ui/configGeneral.qml:101
#, fuzzy, kde-format
#| msgid " days"
msgctxt "@item:valuesuffix spacing to number + unit"
msgid "day"
msgid_plural "days"
msgstr[0] " dienas"
msgstr[1] " dienas"
msgstr[2] " dienas"

#: package/contents/ui/configGeneral.qml:106
#, fuzzy, kde-format
#| msgid "Check for new comic strips:"
msgctxt "@label:spinbox"
msgid "Check for new comics every:"
msgstr "Pārbaudīt jaunas komiksa lentas:"

#: package/contents/ui/configGeneral.qml:115
#, fuzzy, kde-format
#| msgid " minutes"
msgctxt "@item:valuesuffix spacing to number + unit (minutes)"
msgid "minute"
msgid_plural "minutes"
msgstr[0] " minūtes"
msgstr[1] " minūtes"
msgstr[2] " minūtes"

#: stripselector.cpp:31 stripselector.cpp:101
#, fuzzy, kde-format
#| msgid "Go to Strip"
msgctxt "@title:window"
msgid "Go to Strip"
msgstr "Iet uz lentu"

#: stripselector.cpp:39
#, fuzzy, kde-format
#| msgid "&Strip Number:"
msgctxt "@label:spinbox"
msgid "&Strip number:"
msgstr "&Lentas numurs:"

#: stripselector.cpp:102
#, fuzzy, kde-format
#| msgid "Strip identifier:"
msgctxt "@label:textbox"
msgid "Strip identifier:"
msgstr "Lentas identifikators:"

#, fuzzy
#~| msgid "&Create Comic Book Archive..."
#~ msgctxt "@action"
#~ msgid "&Create Comic Book Archive…"
#~ msgstr "&Saglabāt komiksu grāmatas arhīvu..."

#~ msgid "Archiving comic failed"
#~ msgstr "Komiksa arhivēšana neizdevās"

#, fuzzy
#~| msgid "Create %1 Comic Book Archive"
#~ msgctxt "@title:window"
#~ msgid "Create %1 Comic Book Archive"
#~ msgstr "Izveidot %1 komiksu grāmatas arhīvu"

#~ msgid "Destination:"
#~ msgstr "Mērķis:"

#~ msgid "*.cbz|Comic Book Archive (Zip)"
#~ msgstr "*.cbz|Komiksu grāmatas arhīvs (Zip)"

#~ msgid "The range of comic strips to archive."
#~ msgstr "Komiksa lentu diapozons, ko arhivēt."

#~ msgid "Range:"
#~ msgstr "Diapozons:"

#~ msgid "All"
#~ msgstr "Visi"

#, fuzzy
#~| msgid "From beginning to ..."
#~ msgid "From beginning to …"
#~ msgstr "No sākuma līdz..."

#, fuzzy
#~| msgid "From end to ..."
#~ msgid "From end to …"
#~ msgstr "No beigām līdz..."

#~ msgid "Manual range"
#~ msgstr "Pašrocīgi iestatīts diapozons"

#~ msgctxt "in a range: from to"
#~ msgid "From:"
#~ msgstr "No:"

#~ msgctxt "in a range: from to"
#~ msgid "To:"
#~ msgstr "Līdz:"

#~ msgid "dd.MM.yyyy"
#~ msgstr "dd.MM.ggg"

#~ msgid "No zip file is existing, aborting."
#~ msgstr "Nav neviena zip faila, pārtrauc."

#~ msgid "An error happened for identifier %1."
#~ msgstr "Pēc identifikatora %1 radās kļūda."

#~ msgid "Failed creating the file with identifier %1."
#~ msgstr "Neizdevās izveidot failu ar identifikatoru %1."

#~ msgid "Creating Comic Book Archive"
#~ msgstr "Izveido komiksu grāmatas arhīvu"

#~ msgid "Failed adding a file to the archive."
#~ msgstr "Neizdevās pievienot failus arhīvam."

#~ msgid "Could not create the archive at the specified location."
#~ msgstr "Neizdevās izveidot arhīvu norādītajā vietā."

#~ msgid "Getting comic strip failed:"
#~ msgstr "Komiksa lentas saņemšana neizdevās:"

#~ msgid ""
#~ "Maybe there is no Internet connection.\n"
#~ "Maybe the comic plugin is broken.\n"
#~ "Another reason might be that there is no comic for this day/number/"
#~ "string, so choosing a different one might work."
#~ msgstr ""
#~ "Varbūt nav savienojuma ar internetu.\n"
#~ "Varbūt komiksu spraudnis ir bojāts.\n"
#~ "Vēl viens iespējamais iemesls — nav komiksa ar šo dienu/numuru/lentu, "
#~ "tādēļ varat mēģināt izvēlēties citu."

#~ msgid ""
#~ "\n"
#~ "\n"
#~ "Choose the previous strip to go to the last cached strip."
#~ msgstr ""
#~ "\n"
#~ "\n"
#~ "Izvēlieties iepriekšējo lentu, lai pārietu uz pēdējo kešatmiņā saglabāto "
#~ "lentu."

#, fuzzy
#~| msgid "Download new comics"
#~ msgctxt "@title:window"
#~ msgid "Download Comics"
#~ msgstr "Lejupielādēt jaunus komiksus"

#, fuzzy
#~| msgctxt "refers to caching of files on the users hd"
#~| msgid "Cache"
#~ msgctxt "@title:group"
#~ msgid "Cache"
#~ msgstr "Kešatmiņa"

#, fuzzy
#~| msgid "Error Handling"
#~ msgctxt "@title:group"
#~ msgid "Error Handling"
#~ msgstr "Kļūdu apstrāde"

#, fuzzy
#~| msgid "Show arrows only on &hover:"
#~ msgctxt "@option:check"
#~ msgid "Show arrows only on mouse-over"
#~ msgstr "Rādīt bultas tikai ja &pele virsū:"

#, fuzzy
#~| msgid "Information"
#~ msgctxt "@title:group"
#~ msgid "Information"
#~ msgstr "Informācija"

#, fuzzy
#~| msgid "Show comic &URL:"
#~ msgctxt "@option:check"
#~ msgid "Show comic URL"
#~ msgstr "Rādīt komiksa &URL:"

#, fuzzy
#~| msgid "Update"
#~ msgctxt "@title:group"
#~ msgid "Update"
#~ msgstr "Atjaunināt"

#, fuzzy
#~| msgid "Automatically update comic plugins:"
#~ msgctxt "@label:spinbox"
#~ msgid "Automatically update comic plugins:"
#~ msgstr "Automātiski atjaunināt komiksa spraudņus:"

#, fuzzy
#~| msgid "Appearance"
#~ msgctxt "@title:group"
#~ msgid "Appearance"
#~ msgstr "Izskats"

#~ msgid "No size limit"
#~ msgstr "Bez izmēra ierobežojuma"

#~ msgid "every "
#~ msgstr "katras "

#~ msgid "never"
#~ msgstr "nekad"

#~ msgid "Maximum &Size of Widget"
#~ msgstr "Maksimālais sīkrīka &izmērs"

#~ msgid "Tabbar"
#~ msgstr "Ciļņu josla"

#~ msgctxt "Tabbar will show text only"
#~ msgid "Text only"
#~ msgstr "Tikai teksts"

#~ msgctxt "Tabbar will show icons only"
#~ msgid "Icons only"
#~ msgstr "Tikai ikonas"

#~ msgctxt "Tabbar will show both text and icons"
#~ msgid "Text and Icons"
#~ msgstr "Teksts un ikonas"

#~ msgid ""
#~ "Show at actual size in a different view.  Alternatively, click with the "
#~ "middle mouse button on the comic."
#~ msgstr ""
#~ "Rādīt īstajā izmērā atsevišķā skatā. Alternatīvi, nospiediet vidējo peles "
#~ "pogu uz komiksa."

#~ msgid "Press the \"Get New Comics ...\" button to install comics."
#~ msgstr ""
#~ "Nospiediet pogu \"Ielādēt jaunus komiksus ...\", lai instalētu komiksus."

#~ msgid "Tabs"
#~ msgstr "Cilnes"

#~ msgid "Use &tabs:"
#~ msgstr "Lietot &cilnes:"

#~ msgid "Pressing Ctrl and scrolling also changes the tabs."
#~ msgstr "Nospiežot Ctrl un ritinot pārslēdz cilnes."

#~ msgid "&Hide Tabbar:"
#~ msgstr "&Slēpt ciļņu joslu:"

#~ msgid "Interacting with the comic widget restarts the timer."
#~ msgstr "Darbošanās ar komiksu sīkrīku nomet taimeri."

#~ msgid "hh 'Hours' mm 'Mins' ss 'Secs'"
#~ msgstr "hh 'Stundas' mm 'Minūtes' ss 'Sekundes'"

#~ msgid "Automatically &switch tabs:"
#~ msgstr "Automātiski &pārslēgt cilnes:"
