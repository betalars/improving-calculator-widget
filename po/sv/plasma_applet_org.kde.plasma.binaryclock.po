# translation of plasma_applet_binaryclock.po to Swedish
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2007, 2008, 2009, 2017, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_binaryclock\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-09-25 02:25+0200\n"
"PO-Revision-Date: 2019-03-16 19:54+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/config/config.qml:17
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Utseende"

#: package/contents/ui/configGeneral.qml:35
#, kde-format
msgid "Display:"
msgstr "Visning:"

#: package/contents/ui/configGeneral.qml:36
#: package/contents/ui/configGeneral.qml:90
#, kde-format
msgctxt "@option:check"
msgid "Grid"
msgstr "Rutnät"

#: package/contents/ui/configGeneral.qml:41
#: package/contents/ui/configGeneral.qml:77
#, kde-format
msgctxt "@option:check"
msgid "Inactive LEDs"
msgstr "Avstängda lysdioder"

#: package/contents/ui/configGeneral.qml:46
#, kde-format
msgctxt "@option:check"
msgid "Seconds"
msgstr "Sekunder"

#: package/contents/ui/configGeneral.qml:51
#, kde-format
msgctxt "@option:check"
msgid "In BCD format (decimal)"
msgstr "Med BCD-format (decimalt)"

#: package/contents/ui/configGeneral.qml:60
#, kde-format
msgid "Use custom color for:"
msgstr "Använd egen färg för:"

#: package/contents/ui/configGeneral.qml:64
#, kde-format
msgctxt "@option:check"
msgid "Active LEDs"
msgstr "Aktiva lysdioder"

#~ msgctxt "@option:check"
#~ msgid "Draw grid"
#~ msgstr "Rita rutnät"

#~ msgctxt "@option:check"
#~ msgid "Show inactive LEDs"
#~ msgstr "Visa avstängda lysdioder"

#~ msgctxt "@option:check"
#~ msgid "Display seconds"
#~ msgstr "Visa sekunder"

#~ msgctxt "@title:group"
#~ msgid "Colors"
#~ msgstr "Färger"

#~ msgctxt "@option:check"
#~ msgid "Use custom color for active LEDs"
#~ msgstr "Använd egen färg på aktiva lysdioder"

#~ msgctxt "@option:check"
#~ msgid "Use custom color for inactive LEDs"
#~ msgstr "Använd egen färg på avstängda lysdioder"

#, fuzzy
#~| msgid "Appearance"
#~ msgctxt "@title:group"
#~ msgid "Appearance"
#~ msgstr "Utseende"

#~ msgid "Check this if you want to see the inactive LEDs."
#~ msgstr "Markera om du vill se de avstängda lysdioderna."

#~ msgid "Show"
#~ msgstr "Visa"

#~ msgid "Use theme color"
#~ msgstr "Använd temats färg"

#~ msgid "Show the grid"
#~ msgstr "Visa rutnätet"

#~ msgid "Check this if you want to see a grid around leds."
#~ msgstr "Markera om du vill se ett rutnät omkring lysdioderna."

#~ msgid "Use custom grid color:"
#~ msgstr "Använd egen färg på rutnät:"

#~ msgid "Information"
#~ msgstr "Information"

#~ msgid "Show the seconds LEDs"
#~ msgstr "Visa sekundlysdioderna"

#~ msgid ""
#~ "Check this if you want to display seconds LEDs in order to see the "
#~ "seconds."
#~ msgstr ""
#~ "Markera om du vill visa sekundlysdioder för att kunna se sekunderna."

#~ msgid "General"
#~ msgstr "Allmänt"
